import java.util.* ;
import java.awt.Color ;

/**
 * @author achro
 *
 */
public class Cycliste
{

	/**
	 * Liste de cartes qui repr�sente la pioche d'un joueur pour son Cycliste (Sprinter ou Rouleur)
	 */
	protected LinkedList<Carte> pioche ;

	/**
	 * Liste de carte repr�sentant la d�fausse d'un chaque joueur pour chacun de ses cyclistes
	 * Elle sera remise dans la pioche quand la pioche sera vide
	 */
	protected LinkedList<Carte> defausse ;

	/**
	 * Equipe du Cylciste (Vert/Bleu/Noir/Rouge)
	 */
	private Color couleurEquipe ;

	public Color getColor()
	{ return this.couleurEquipe ; }



	/**
	 * Constructeur de la classe Cycliste qui sera utilis� au d�but de la partie
	 * @param c Couleur de l'�quipe du Cycliste
	 */
	public Cycliste(Color c){
		this.pioche = new LinkedList<Carte>() ;
		this.defausse = new LinkedList<Carte>() ;
		this.couleurEquipe = c ;
	}

	/**
	 * M�thode ajoutDsPioche 
	 * permettant d'ajouter une carte au d�but de la pioche
	 * @param c carte � ajouter
	 */
	public void ajoutDsPioche(Carte c){
		if ( c != null )
		{ this.pioche.addFirst(c) ; }
	}



	/**
	 * M�thode piocherMain
	 * permet au Joueur de piocher les 4 cartes qui seront dans sa main
	 * et qu'il pourra choisir pour faire avancer son cycliste
	 * @return
	 */
	public LinkedList<Carte> piocherMain(){
		LinkedList<Carte> main = new LinkedList<Carte>() ;
		for (int i=0 ; i<4 ; i++)
		{
			main.add(this.pioche.pollFirst()) ;
		}
		return main ;
	}


	/**
	 * M�thode afficherMain
	 * permet d'afficher la main piocher par le joueur pour son cycliste
	 * @return Une chaine de caract�re permettant de connaitre les cartes
	 */
	public String afficherMain()
	{
		String s = "Main : \n" ;
		for (int i=0 ; i<(this.pioche.size()) ; i++){
			s += "Carte n�"+i+" "+this.pioche.get(i).numero + "\n" ;
		}
		return s ;
	}

	/*
	 * M�thode afficherDefausse
	 * permettant d'afficher la defausse d'un joueur pour son cylciste
	 * @return Une chaine de caract�re permettant de savoir quelles cartes se trovuent dans la defausse
	 */
	public String afficherDefausse()
	{
		String s = "D�fausse : \n" ;
		for (int i=0 ; i<(this.defausse.size()) ; i++)
		{ s += this.defausse.get(i).numero + " " ; }
		return s ;
	}


	public String toString()
	{
		String s = "" ;

		if ( this instanceof Sprinter ) {
			s = "S" ;
		}else if ( this instanceof Rouleur ) {
			s = "R" ;
		}
		if (this.couleurEquipe.equals(Color.RED)) {
			s += "-R" ;
		}else if ( this.couleurEquipe.equals(Color.GREEN)){
			s += "-V" ;
		}else if ( this.couleurEquipe.equals(Color.BLACK)){
			s += "n-N" ;
		}else if ( this.couleurEquipe.equals(Color.BLUE)){
			s += "-B" ;
		}
		return s ;
	}


	/**
	 * M�thode estLeBonCylciste
	 * permettant de savoir si le cyclsite selectionn� 
	 * est le bon cycliste
	 * @param c Cycliste selectionn�
	 * @return res Bool�en � true si le cylciste est le bon
	 */
	public boolean estLeBonCycliste(Cycliste c){
		boolean res = false;
		if ( c == null ) {
			res = false ;
		}
		if ( (this instanceof Sprinter) && (c instanceof Sprinter) ){
			if ( this.getColor().equals(c.getColor()) ) {
				res = true ;
			}
		}else if ( (this instanceof Rouleur) && (c instanceof Rouleur)){
			if ( this.getColor().equals(c.getColor()) ) {
				res = true ;
			}
		}
		return false ;
	}
}


import java.awt.Color ;
import java.util.Collections;

/**
 * @author VIARD et ALVES
 *
 */
public class Rouleur extends Cycliste{

	/**
	 * Constructeur de Rouleur s'appuyant sur la classe Cycliste
	 * @param c couleur du Rouleur 
	 */
	public Rouleur(Color c){
		super(c) ;  
		for (int i=0 ; i<15 ; i++){
			if(i<3) {
				super.ajoutDsPioche(new Carte(3)); 
			}else if(i>=3 && i<6) {
				super.ajoutDsPioche(new Carte(4)); 
			}else if(i>=6 && i<9) {
				super.ajoutDsPioche(new Carte(5)); 
			}else if(i>=9 && i<12) {
				super.ajoutDsPioche(new Carte(6)); 
			}else if(i>=12 && i<15) {
				super.ajoutDsPioche(new Carte(7)); 
			}
		}
		Collections.shuffle(this.pioche);//M�thode shuffle de COllection qui permet de m�langer une Colelction 
		//(ici une Linked Liste)
	}
}

import java.awt.Color ;

/**
 * @author VIARD et ALVES
 *
 */
public class Joueur{

	/**
	 * Attribut nom du Joueur
	 * C'est son nom
	 */
	private String nom ;
	
	/**
	 * Attribut couleur du Joueur
	 * C'est la couleur de son �quipe (Bleu, Vert, Rouge, Noir)
	 */
	private Color couleur ;
	
	/**
	 * Attribut rouleur du Joueur
	 * C'est son cycliste Rouleur 
	 */
	protected Rouleur rouleur ;
	
	/**
	 * Attribut sprinteur du Joueur
	 * C'est son Cycliste Sprinter 
	 */
	protected Sprinter sprinter ;

	/**
	 * Constructeur de la classe Joueur 
	 * @param s Nom du Joueur
	 * @param c Couleur de l'�quipe choisie par le Joueur
	 */
	public Joueur(String s , Color c){
		this.nom = s ;
		this.couleur = c ;
		this.rouleur = new Rouleur(c) ;
		this.sprinter = new Sprinter(c) ;
	}


	/**
	 * Constructeur par d�faut de la classe Joueur
	 * Il initialise le nom � une chaine vide
	 * et la couleur � Noir
	 */
	public Joueur(){
		this.nom = " " ;
		this.couleur = Color.GREEN;
		this.rouleur = new Rouleur(Color.GREEN) ;
		this.sprinter = new Sprinter(Color.GREEN) ;
	}


	public String getNomJoueur(){
		return this.nom ;
	}

	public Color getColor(){
		return this.couleur ; 
	}

	public String toString(){
		return ("Le Joueur : "+this.nom + " a la couleur " + this.couleur);
	}
}

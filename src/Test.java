import java.util.* ;
import java.awt.Color ;
import static libtest.Lanceur.lancer ;
import static libtest.OutilTest.assertEquals ;

public class Test
{
  /**
  * 28 03 2018
  * Test du bon fonctionnement des classes Cyclistes et Cartes ( et leurs classes filles )
  */
  // Test de l'init du rouleur
  public  void test_00_InitRouleur(){
    Rouleur rouleur1 = new Rouleur(Color.BLACK) ;
    assertEquals("La main du joueur devrait avoir ete melangee" , false , rouleur1.afficherMain().equals("3 3 3 4 4 4 5 5 5 6 6 6 7 7 7")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 3" , true , rouleur1.afficherMain().contains("3")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 4" , true , rouleur1.afficherMain().contains("4")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 5" , true , rouleur1.afficherMain().contains("5")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 6" , true , rouleur1.afficherMain().contains("6")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 7" , true , rouleur1.afficherMain().contains("7")) ;
    assertEquals("La defausse du joueur devrait etre vide" , "" , rouleur1.afficherDefausse()) ;
  }


  // Test de l'init du sprinter
  public  void test_01_InitSprinter(){
    Sprinter sprinter1 = new Sprinter(Color.BLACK) ;
    assertEquals("La main du joueur devrait avoir ete melangee" , false , sprinter1.afficherMain().equals("2 2 2 3 3 3 4 4 4 5 5 5 9 9 9")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 2" , true , sprinter1.afficherMain().contains("2")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 3" , true , sprinter1.afficherMain().contains("3")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 4" , true , sprinter1.afficherMain().contains("4")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 5" , true , sprinter1.afficherMain().contains("5")) ;
    assertEquals("Un type de carte energie a disparue de la main du cycliste : la 7" , true , sprinter1.afficherMain().contains("9")) ;
    assertEquals("La defausse du joueur devrait etre vide" , "" , sprinter1.afficherDefausse()) ;
  }


  // test de pioche de 4 Cartes
  public void test_02_piocheCartes()
  {
    Sprinter sprinter1 = new Sprinter(Color.BLACK) ;

    String piocheAOrigine = sprinter1.afficherMain() ;

    LinkedList<Carte> pioche = sprinter1.piocherMain() ;
    String choixCarte = "" ;
    for (int i=0 ; i<4 ; i++)
    { choixCarte += pioche.get(i).numero + " " ; }

    String piocheApres = sprinter1.afficherMain() ;

    assertEquals("Les cartes choisit aurrais due etre retiree de la pioche" , false , piocheAOrigine.equals(piocheApres)) ;
    assertEquals("Les cartes retiree aurait due a l origine dans la pioche" , true , piocheAOrigine.contains(choixCarte)) ;
  }


  /**
  * ---- 01/04/2018 ----
  * testes de la méthode principale
  */

  // test de l'initialisation du plateau
  public void test_03_InitPlateau()
  {
    String[] nomJ = {"Marc" , "Philipe"} ;
    Color[] couleurJ = {Color.RED , Color.BLUE} ;
    Jeux game = new Jeux(2 , nomJ , couleurJ) ;

    assertEquals("Il ne devrait y avoir que 2 joueurs" , true , game.getNbJoueur() == 2) ;
    for (int i=0 ; i<game.getNbJoueur() ; i++)
    {
      assertEquals("Il devrait y avoir un cycliste sprinter ici" , true , game.getCase(i)[0] instanceof Sprinter) ;
      assertEquals("Il devrait y avoir un cycliste rouleur ici" , true , game.getCase(i)[1] instanceof Rouleur) ;
    }

    for (int j=2 ; j<50 ; j++)
    {
      assertEquals("Il ne devrait rien avoir ici" , true , game.getCase(j)[0] == null ) ;
      assertEquals("Il ne devrait rien avoir ici" , true , game.getCase(j)[1] == null ) ;
    }
  }


  /**public static void main(String[] args)
  { lancer(new Test(),args) ; } */

  public static void main(String[] args) 
  { Jeux game = new Jeux() ; }
}

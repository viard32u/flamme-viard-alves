/**
 * @author VIARD et ALVES
 *
 */
public class Carte{
  /**
  * Nombre de déplacement que permet la carte
  */
  protected int numero ;

  /**
   * Constructeur de la classe Carte
 * @param p numero qui apparaitra sur la carte
 */
public Carte(int n){
	this.numero = n ;
  }
  
  public String toString()
  { return this.numero + "" ; }
}

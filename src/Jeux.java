import java.util.* ;
import java.awt.Color ;

/**
 * @author VIARD et ALVES
 *
 */
public class Jeux{
	/**
	 * Attribut joueurs du Jeu
	 * c'est la liste des joueurs
	 */
	private LinkedList<Joueur> joueurs ;
	/**
	 * attribut circuit du Jeu 
	 * c'est un tableau de Cylciste repr�sentant l'�tape totale
	 */
	private ArrayList<Cycliste[]> circuit ;

	/**
	 * Atribut Scanner de la classe Jeux il permettra de saisir les diff�rentes commandes pour le joueur
	 */
	private static Scanner sc = new Scanner(System.in);


	public Cycliste[] getCase(int index){
		if ( index>=0 && index<=this.circuit.size()) {
			return this.circuit.get(index) ;
		}else {
			return null ;
		}
	}


	public int getNbJoueur()
	{ return this.joueurs.size() ; }



	/**
	 * Constructeur principalement utilise pour les tests
	 * qui permet de se passer des commande entr�es par les joueurs 
	 */
	public Jeux(int nbJ , String[] nomJ , Color[] couleurJ){
		this.circuit = new ArrayList<Cycliste[]>() ;
		Cycliste[] tabTemp ;
		for (int i=0 ; i<50 ; i++)
		{
			tabTemp = new Cycliste[2] ;
			this.circuit.add(tabTemp) ;
		}
		this.joueurs = new LinkedList<Joueur>() ;
		for (int i=0 ; i<nbJ ; i++) {
			this.joueurs.addFirst(new Joueur(nomJ[i] , couleurJ[i])) ;
		}
		this.placerDepart() ;
	}



	/**
	 * Constructeur par defaut 
	 * permettant de cr�er les pions en fonctions des choix de chaque joueurs
	 */
	public Jeux() throws InputMismatchException{
		this.circuit = new ArrayList<Cycliste[]>() ;
		Cycliste[] tabTemp ;
		for (int i=0 ; i<50 ; i++)		{
			tabTemp = new Cycliste[2] ;
			this.circuit.add(tabTemp) ;
		}
		System.out.print("Combien de Joueur �tes vous ? ");
		int nbJ = insertInt(2 , 4) ;
		ArrayList<String> choixCouleur = new ArrayList<String>() ;
		choixCouleur.add("Rouge") ;
		choixCouleur.add("Bleu") ;
		choixCouleur.add("Vert") ;
		choixCouleur.add("Noir") ;
		ArrayList<Color>  couleur = new ArrayList<Color>() ;
		couleur.add(Color.RED) ;
		couleur.add(Color.BLUE) ;
		couleur.add(Color.GREEN) ;
		couleur.add(Color.BLACK) ;
		this.joueurs = new LinkedList<Joueur>() ;
		String tempNomJ;
		String tempCouleurJ ;
		boolean incorrecte = false ;
		int rang = 0 ;
		for (int i=1 ; i<nbJ+1 ; i++){
			tempNomJ = "" ;
			while (tempNomJ.equals("")){
				System.out.print("Quel est le nom du Joueur " + (i) + " ? ") ;
				tempNomJ = sc.next() ;
			}
			if ( choixCouleur.size() > 1 ){
				while ( !(incorrecte) ){
					System.out.println("Quelle couleur choisissez vous ? (Rouge, Bleu, Vert, Noir)") ;
					String c = "(" ;
					tempCouleurJ = sc.next() ;
					rang = 0 ;
					for ( String s : choixCouleur )   // verification que la valeur entree par l'utilisateur fait bien partit des choix propose
					{
						if (tempCouleurJ.equals(s))
							incorrecte = true ;
						else
							if ( !(incorrecte) )
								rang++ ;
					}
				}
			}else {
				rang = 0 ;
			}
			this.joueurs.addFirst( new Joueur(tempNomJ , couleur.get(rang)) ) ;
			couleur.remove(rang) ;
			choixCouleur.remove(rang) ;
			incorrecte = false ;
		}
		this.placerDepart() ;
	}



	/**
	 * Methode permettant de mettre tous les cyclistes � leur position de d�part
	 *
	 */
	public void placerDepart()
	{
		for (int i=0 ; i<this.joueurs.size() ; i++){
			Cycliste[] temp = new Cycliste[2] ;
			temp[0] = this.joueurs.get(i).sprinter ;
			temp[1] = this.joueurs.get(i).rouleur ;
			this.circuit.add(i , temp) ;
		}
	}




	/**
	 * M�thode permettant de savoir si le jeu est fini
	 * @return un bool�en � True si le jeu est terminer et qu'un joueur � gagner
	 */
	public boolean estFini()
	{
		boolean arrivee = false ;
		if ( this.circuit.get(this.circuit.size()-1)[0] != null || this.circuit.get(this.circuit.size()-1)[1] != null )
			arrivee = true ;

		return arrivee ;
	}



	/**
	 * M�thode permettant de connaitre le classement des cyclistes
	 * @return Une liste de cycliste repr�sentant leur calssement actuel sur le terrain
	 */
	private LinkedList<Cycliste> listeCyclisteTrierParPosition(){
		LinkedList<Cycliste> lCyclisteTrier = new LinkedList<Cycliste>() ;
		int nbCyclisteTrouvee = 0 ;
		int i = 0 ;

		while ( nbCyclisteTrouvee != (this.joueurs.size()*2) && i < (this.circuit.size()-1) )
		{
			if ( this.circuit.get(i)[0] != null ) // il y a quelqu'un sur cette case
			{
				lCyclisteTrier.addFirst(this.circuit.get(i)[0]) ;
				nbCyclisteTrouvee++ ;
			}

			if ( this.circuit.get(i)[1] != null ) // il y a quelqu'un sur cette case
			{
				lCyclisteTrier.addFirst(this.circuit.get(i)[1]) ;
				nbCyclisteTrouvee++ ;
			}

			i++ ;
		}

		if (nbCyclisteTrouvee != (this.joueurs.size()*2))
			System.out.println("ERROR: tout les cyclistes n'ont pas ete trouve") ;

		return lCyclisteTrier ;
	}



	/**
	 * Methode qui d�finit l'ordre des joueurs
	 * Le premier joueur se retrouve au d�but de la list JoueursTrier
	 */
	public void ordreJeuxJoueur(){
		LinkedList<Joueur> joueursTrier = new LinkedList<Joueur>() ;
		LinkedList<Cycliste> lCyclisteTrier = this.listeCyclisteTrierParPosition() ;
		boolean equipeDejaPresente ;

		for (Cycliste c : lCyclisteTrier)
		{
			equipeDejaPresente = false ;
			for ( Joueur j : joueursTrier )
				if ( (j.getColor()).equals(c.getColor()))
					equipeDejaPresente = true ;

			if ( !(equipeDejaPresente) )
				for (Joueur j : this.joueurs)
					if ( j.getColor().equals(c.getColor()) )
						joueursTrier.add(j) ;
		}

		this.joueurs = new LinkedList<Joueur>(joueursTrier) ;
	}



	/**
	 * Methode repr�sentant la phase Energie,
	 * Ou le Joueur choisit quelle carte il veut jouer parmis les 4 qu'il a piocher pour chaque cyclistes
	 * 
	 */
	public void phaseEnergie()
	{

		String nomCycliste = "" ;
		LinkedList<int[]> choixCartesJoueurs = new LinkedList<int[]>() ;
		for ( Joueur j : this.joueurs )
		{
			System.out.println("\nTours de " + j.getNomJoueur()) ;
			System.out.println("Par lequel de vos cyclistes souhaitez vous commencer? ") ;
			System.out.print("(rouleur ou sprinter) ") ;
			while ( nomCycliste.equals("") || !(nomCycliste.equals("rouleur") || nomCycliste.equals("sprinter")) )		// attention test peut etre faux -> imbriquement louche
				nomCycliste = sc.next() ;

			int[] choixCartes = new int[2] ;
			if ( nomCycliste.equals("rouleur") )
			{
				choixCartes[0] = phaseEnergieCycliste(j.rouleur) ;
				choixCartes[1] = phaseEnergieCycliste(j.sprinter) ;
			}
			else if ( nomCycliste.equals("sprinter") )
			{
				choixCartes[1] = phaseEnergieCycliste(j.sprinter) ;
				choixCartes[0] = phaseEnergieCycliste(j.rouleur) ;
			}
			else
				System.out.println("ERROR: entree innatendue") ;

			choixCartesJoueurs.add(choixCartes) ;
			nomCycliste = "" ;
		}
		Iterator<Joueur> cursJ = this.joueurs.iterator() ;
		Iterator<int[]> curs = choixCartesJoueurs.iterator() ;

		for ( int i=0 ; i<this.getNbJoueur() ; i++ )
		{
			int[] temp = curs.next() ;
			Joueur j = cursJ.next() ;  
			faireAvancerCycliste( j.rouleur , temp[0] ) ;
			faireAvancerCycliste( j.sprinter , temp[1] ) ;
		}
	}



	/**
	 * M�thode repr�sentant la phase o� l'aspiration s'effectue sur le Plateau
	 */
	public void phaseAspiration()
	{
		LinkedList<Cycliste> lCyclistes = listeCyclisteTrierParPosition() ;

		for (int i = lCyclistes.size() - 1 ; i>=0 ; i--)
		{
			Cycliste cTemp = lCyclistes.get(i) ;
			int cTempPlace = getPositionCycliste(cTemp) ;

			if ( this.circuit.get(cTempPlace)[0] != null )
				if ( this.circuit.get(cTempPlace)[0].estLeBonCycliste(cTemp) )
					if ( cTempPlace+2 < this.circuit.size()-1 )
						if ( this.circuit.get(cTempPlace+2)[0] != null )
							faireAvancerCycliste(cTemp , cTempPlace+1) ;

			if ( this.circuit.get(cTempPlace)[1] != null )
				if ( this.circuit.get(cTempPlace)[1].estLeBonCycliste(cTemp) )
					if ( cTempPlace+2 < this.circuit.size()-1 )
						if ( this.circuit.get(cTempPlace+2)[1] != null )
							faireAvancerCycliste(cTemp , cTempPlace+1) ;
		}
	}

	/**
	 * M�thode qui permet au joueur de s�lectionner la carte dans sa main
	 * @param c Cycliste du Joueur
	 * @return Carte choisie par le joueur sous la forme d'un INT
	 */
	private int phaseEnergieCycliste(Cycliste c)
	{
		LinkedList<Carte> choixCartes = c.piocherMain() ;

		String sChoixCarte = "" ;
		for ( Carte uneCarte : choixCartes )
			sChoixCarte += uneCarte + " " ;

		System.out.print("Voici les cartes d energie que vous avez piochees: " + sChoixCarte) ;
		String sInput = "" ;
		while ( sInput.equals("") || !(sChoixCarte.contains(sInput)) )
			sInput = sc.next() ;

		System.out.println(Integer.parseInt(sInput));
		return Integer.parseInt(sInput) ;
	}

	/**
	 * Methode permettant de connaire la position d'un cycliste sur le plateau de jeu
	 * @param c Le cylciste recherch�
	 * @return l'index de sa position dans le tableau
	 */
	private int getPositionCycliste(Cycliste c)
	{
		int i = -1 ;
		if ( c != null )
		{
			boolean trouvee = false ;
			while ( !trouvee && i<this.circuit.size() )
			{
				i++ ;
				Cycliste[] cases = this.circuit.get(i) ;

				if ( cases[0] != null )
					if ( cases[0].estLeBonCycliste(c) )
						trouvee = true ;

				if (cases[1] != null)
					if ( cases[1].estLeBonCycliste(c) )
						trouvee = true ;
			}
		}

		System.out.print(i + " ") ;
		return i ;
	}


	/**
	 * Permet de faire se d�placer un cycliste sur le plateau
	 * @param c Cycliste qui doit avancer
	 * @param nbCaseAvance Entier qui repr�sente le nombre que case parcourue par le cylciste sur le terrain
	 */
	private void faireAvancerCycliste(Cycliste c , int nbCaseAvance)
	{
		int positionDuCycliste = this.getPositionCycliste(c) ;
		if ( positionDuCycliste + nbCaseAvance >= this.circuit.size() )
			nbCaseAvance = this.circuit.size() - positionDuCycliste - 1 ;
		Cycliste[] ancienneCase = new Cycliste[2] ;
		if ( this.circuit.get(positionDuCycliste)[1] != null )
			if (this.circuit.get(positionDuCycliste)[1].estLeBonCycliste(c))
				ancienneCase[0] = this.circuit.get(positionDuCycliste)[0] ;
			else 
				ancienneCase[0] = this.circuit.get(positionDuCycliste)[1] ;

		this.circuit.set(positionDuCycliste, ancienneCase) ;
		Cycliste[] temp = this.circuit.get(positionDuCycliste+nbCaseAvance) ;
		Cycliste[] nouvelleCase = new Cycliste[2] ;
		if ( temp[0] != null )
		{
			nouvelleCase[0] = temp[0] ;
			nouvelleCase[1] = c ;
		}
		else if (temp[1] == null)
			nouvelleCase[0] = c ;
		else
		{
			int i = 1 ;
			while ( this.circuit.get(positionDuCycliste+nbCaseAvance-i)[0] != null && this.circuit.get(positionDuCycliste+nbCaseAvance-i)[1] != null  && positionDuCycliste-i>0 )
				i++ ;

			if ( i == positionDuCycliste )
				System.out.println("ERREUR: impossible de replacer le cycliste") ;

			if ( this.circuit.get(positionDuCycliste+nbCaseAvance-i)[0] != null)
			{
				nouvelleCase[0] = this.circuit.get(positionDuCycliste+nbCaseAvance-i)[0] ;
				nouvelleCase[1] = c ;
			}
			else
				nouvelleCase[0] = c ;
		}

		this.circuit.set(positionDuCycliste+nbCaseAvance , nouvelleCase) ;
	}

	/**
	 * Affichage du circuit
	 * @return String qui repr�sente le circuit
	 */
	public String affichageTerrrain() 
	{  

		String lignesCote = "" ;
		String joueursLGauche = "" ;
		String ligneCentrale = "" ;
		String joueursLDroite = "" ;

		for ( int i=0 ; i<this.circuit.size() ; i++)
		{
			lignesCote += "____" ;

			ligneCentrale += "|-|-|-|-|" ;

			if ( this.circuit.get(i)[0] != null )
				joueursLDroite += " " + this.circuit.get(i)[0] + " " ;
			else
				joueursLDroite += "    " ;

			if ( this.circuit.get(i)[1] != null )
				joueursLGauche += " " + this.circuit.get(i)[1] + " " ;
			else
				joueursLGauche += "    " ;
		}

		String s = lignesCote + "\n" + lignesCote + "\n" + joueursLGauche + "\n" + ligneCentrale + "\n" + joueursLDroite + "\n" + lignesCote + "\n" + lignesCote ;
		return s ;
	}



	/**
	 * <p> Methode permettant a l'utilisateur d'inserer un entier, il devrat etre compris entre les deux autre entiers entree dans la methode. </p>
	 * <p> Encadre bien l'utilisateur de sorte a eviter toute erreur ou valeur etrange. </p>
	 *
	 *   @param borneInf
	 *       borne inferieur : la valeur retournee seras forcemment >= a cette valeur
	 *
	 *   @param borneSup
	 *       borne superieur : valeur retounee seras forcemment <= a cette valeur
	 *
	 *   @return un entier compris entre borneInf et borneSup inseree par l'utilisateur
	 *
	 *   @see Jeux.demarrerJeu
	 */
	private int insertInt(int borneInf , int borneSup) throws InputMismatchException
	{
		int nb = 0 ;
		try
		{
			nb = sc.nextInt() ;
			while ( !(nb <= borneSup && nb >= borneInf) )
			{
				System.out.print("Veuillez rentre un chiffre entre " + borneInf + " et " + borneSup + " s'il-vous-plait") ;
				nb = sc.nextInt() ;
			}
		} catch(InputMismatchException ex) {
			sc.next() ;
			System.out.print("ERREUR : pas un nombre, veuillez inserer un nombre: ") ;
			nb = insertInt(borneInf , borneSup) ;
		}
		return nb ;
	}
}
